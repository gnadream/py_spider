# py_spider

#### 项目介绍
用于讲解python3实现网络爬虫

#### 软件架构

第一阶段 网页爬虫   对应文件夹 season_one

第二阶段 APP爬虫   对应文件夹 season_two

第三阶段 爬虫框架的应用 对应文件夹 season_three

第四阶段 分布式爬虫 对应文件夹 season_four


#### 安装教程

请求库
- urllib
- requests
- Selenium
- ChromeDriver
- GeckoDriver
- PhantomJS
- aiohttp

解析库
- BeautifulSoup
- lxml
- pyquery
- tesserocr


存储库
- PyMySQL
- PyMongo
- redis-py
- RedisDump

爬虫框架
- pyspider
- Scrapy
- Scrapy-Splash
- Scrapy-Redis

部署相关库
- Docker
- Scrapyd
- Scrapyd-Client
- Scrapyd API
- Scrapyrt
- Gerapy

数据库
- Mysql
- MongoDB
- Redis



#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

